//----------------------------------Bibliothèque---------------------------------------------------
#include <BLEDevice.h>   //Pour le BLE
#include <BLEServer.h>   //Pour le BLE
#include <BLEUtils.h>    //Pour le BLE
#include <BLE2902.h>     //Pour le BLE
#include <ArduinoJson.h> //Pour JSON
#include "FS.h"          //
#include "SPIFFS.h"
#include <Wire.h>             //Pour la temperature
#include <Adafruit_GFX.h>     //Pour l'affichage
#include <Adafruit_SSD1306.h> //Pour l'affichage
//---------------------------------Constante----------------------------------------------------------
#define SERVICE_UART_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"      //Identifiant du service (pour le BLE)
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" //Identifiant des characteristiques (pour le BLE)
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" //Identifiant des descriptions (pour le BLE)
#define TMP102_I2C_ADDRESS 72                                         //Adresse du capteur i2c
#define SCREEN_WIDTH 128                                              //Largeur de l'ecran OLED
#define SCREEN_HEIGHT 64                                              //hauteur de l'ecran OLED
#define SDA_I2C 5
#define SCL_I2C 4
#define FICH_CONFIG "/data/config.conf" //Fichier à tester
//------------------------------Création du pointeur pour le BLE-------------------------------------
BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic = NULL;
BLECharacteristic *pRxCharacteristic = NULL;
//-----------------------------------Variable Globale------------------------------------------------
bool estConnecte = false;
bool etaitConnecte = false;
bool envoieDonnees = false;
char messageJson[100];
char affichage[100];
StaticJsonBuffer<300> JsonBufferConfig;
int etatAffichage;
//----------------------------Parametrage de la biblotheque Adafruit---------------------------------
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

//---------------------------Classe Etat serveur BLE-----------------------------------------
class EtatServeur : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    estConnecte = true; //une personne se connecte
  }

  void onDisconnect(BLEServer *pServer)
  {
    estConnecte = false; //une personne se deconnecte
  }
};
//-------------------Classe permetant de recuperrer et d'afficher une valeur en BLE-----------------
class CharacteristicUART : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristique)
  {
    std::string rxValue = pCharacteristique->getValue(); //recuperation de la valeur envoie en BLE

    //Affichage
    if (rxValue.length() > 0)
    {
      Serial.println("*********");
      Serial.print("Valeur reçu : ");
      //Affichage de la valeur dans la console et copie du message
      for (int i = 0; i < rxValue.length(); i++)
      {
        Serial.print(rxValue[i]);
        messageJson[i] = rxValue[i];
        affichage[i] = rxValue[i];
      }
      Serial.println();
      Serial.println("*********");
      envoieDonnees = true;
    }
  }
};
//------------------Fonction de recuperation et d'affichage de la temperature----------------------
float getTemp102()
{
  //Variable contenant les trames que l'on récupére grace à l'I2C
  unsigned char firstbyte, secondbyte;
  //un int est capable de stocker deux octets, c'est là que nous ajoutons les deux octets ensemble.
  int val;
  //Nous devons ensuite multiplier nos deux octets par 0.0625, mentionné dans la fiche technique.
  float convertedtemp;

  //Envoie de la requete I2C au TMP102
  Wire.requestFrom(TMP102_I2C_ADDRESS, 2);

  //ici nous lisons un octet de chacun des registres de température sur le TMP102
  firstbyte = (Wire.read());
  secondbyte = (Wire.read());

  val = firstbyte;

  if ((firstbyte & 0x80) > 0) //Si la valeur est négative
  {
    val |= 0x0F00; //forsage à 1 sur le 1er octets
  }

  val = ((val) << 4); //décalage de 4 sur la gauche (MSB)

  val |= (secondbyte >> 4); //Décalage de 4 à droite (LSB)

  //Multiplication par 0.0625 de la valeur de la temperature (voir fiche tech TMP102)
  convertedtemp = val * 0.0625;

  //Affichage
  Serial.println();
  Serial.print("La temperature est de : ");
  Serial.print(convertedtemp);
  Serial.println();

  return convertedtemp;
}
//------------------------Installation de l'environement d'execution-------------------------
void setup()
{
  //Vitesse de l'envoie des données
  Serial.begin(9600);
  //Demarrage de la communication I2C sur la broche 5 et 4
  Wire.begin(SDA_I2C, SCL_I2C);

  //Initialisation de l'affichage et gestion des erreurs
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }

  //Creation d'un peripherique BLE et lui donne un nom
  BLEDevice::init("Conteneur 2");

  //Definition du peripherique BLE en tant que serveur
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new EtatServeur());

  //Creation d'un service pour le serveur BLE avec UUID
  BLEService *pServiceUART = pServer->createService(SERVICE_UART_UUID);

  //Definition du service avec UUID et les propriétés (NOTIFICATION)
  pTxCharacteristic = pServiceUART->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);

  //Ajout de la description utilisateur caractèristique
  pTxCharacteristic->addDescriptor(new BLE2902());

  //Definition du service avec UUID et les propriétés (ECRIRE)
  pRxCharacteristic = pServiceUART->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);

  //Appel de la fonction de lecture (afin de pouvoir recuperer les données que l'on nous envoie)
  pRxCharacteristic->setCallbacks(new CharacteristicUART());

  //Demarre le service
  pServiceUART->start();

  //Numerise le BLE afin que les autres appareils en BLE puissent le trouver
  pServer->getAdvertising()->start();

  Serial.println("Bluetooth demarrer");
  Serial.println("En attente de connexion");
  Serial.println();
}

//-----------------------------------------Boucle d'execution-----------------------------
void loop()
{
  char dhtDataString[10];
  float temp;
  //Recuperation de la temperature
  temp = getTemp102();
  etatAffichage = 0;

  //Initialisation du vecteur dhtDataString
  for (int i = 0; i <= 6; i++)
  {
    dhtDataString[i] = 0;
  }

  //Copie de la temperature dans un tableau
  dtostrf(temp, 5, 2, dhtDataString);

  if (envoieDonnees == true)
  {
    bool codErr = false ; 
    File monFichier ; 
    //JSON
    JsonObject &parsed = JsonBufferConfig.parseObject(messageJson);
    String nomUtilisateur = parsed["identifiant"];
    String nbTransport = parsed["numTransport"];
    String mdp = parsed["mpVerifContainer"];
    String Heuredepart = parsed["heuredepart"];

    Serial.print("Numero transport : ");
    Serial.println(nbTransport);
    Serial.print("MDP : ");
    Serial.println(mdp);
    Serial.print("Identifiant : ");
    Serial.println(nomUtilisateur);
    Serial.print("Heure de depart");
    Serial.println(Heuredepart);
    
    String texte = "identifiant : " + nomUtilisateur + "\t";
    String texte1 = "numTransport : " + nbTransport + "\t";
    String texte2 = "mpVerifContainer : " +  mdp + "\t";
    String texte3 = "heuredepart : " +  Heuredepart + "\t";

    //montage de la SPPIFFS
    codErr = SPIFFS.begin(true);
    if (codErr == true)
    {
      //montage OK
      Serial.println("Spiffs OK");

      //Ecriture d'un texte dans le fichier de test
      Serial.println("//------------------------------------------------------------");
      Serial.println("Création d'un fichier");
      monFichier = SPIFFS.open(FICH_CONFIG, "w");
   
      Serial.println("\tFichier dispo");
      monFichier.write((uint8_t *)(texte.c_str()), texte.length());
      monFichier.write((uint8_t *)(texte1.c_str()), texte1.length());
      monFichier.write((uint8_t *)(texte2.c_str()), texte2.length());
      monFichier.write((uint8_t *)(texte3.c_str()), texte3.length());
      monFichier.close();

      //Lecture d'un fichier de test + affichage de son contenu
      Serial.println("//--------------------------------------------------------------");

      Serial.println("Lecture du fichier");
      monFichier = SPIFFS.open(FICH_CONFIG, "r");
      if (monFichier.available())
      {
        Serial.println("\tFichier dispo");
        Serial.println(String("\t") + monFichier.readString());
        monFichier.close();
      }
      else
      {
        //Fichier inconnu
        Serial.println("Erreur ouverture fichier");
      }

      //Démontage du systeme de fichiers
      SPIFFS.end();
    }
    
    envoieDonnees = false;
  }

  // notification
  if (estConnecte)
  {
    etatAffichage = 1;

    //Valeur de la caractéristique du service BLE
    pTxCharacteristic->setValue(dhtDataString);

    //Envoie de la valeur à l'appareil connecter à l'esp
    pTxCharacteristic->notify();

    delay(500);
  }

  // déconnecté ?
  if (!estConnecte && etaitConnecte)
  {
    Serial.println("Un utilisateur s'est deconnecté");
    delay(500);

    //Redemarrage du service
    pServer->startAdvertising();
    Serial.println("Le bluetooth a ete redemarrer");
    Serial.println();
    etaitConnecte = estConnecte;
  }

  // connecté ?
  if (estConnecte && !etaitConnecte)
  {
    Serial.println();
    Serial.println("Un utilisateur est connecté");
    Serial.println();
    Serial.print(dhtDataString);
    Serial.println();
    etaitConnecte = estConnecte;
  }

  switch (etatAffichage)
  {
  case 1: //Connexion en bluetooth avec l'application mobile / Demmarrage de l'enregistrement

    //Vide le buffer contenant ce que l'on afficher précédemment
    display.clearDisplay();

    // setTextSize applique est facteur d'échelle qui permet d'agrandir ou réduire la
    //font
    display.setTextSize(1);

    // La couleur du texte
    display.setTextColor(WHITE);

    // On va écrire en x=0, y=0
    display.setCursor(0, 0);

    // un println comme pour écrire sur le port série
    display.println("12");
    display.println("Wuilmart");
    display.println("78%");

    //Affichage de la valeur recu en BLE
    for (int i = 0; i < 20; i++)
    {
      display.print(affichage[i]);
    }

    //Rafraichire le cache de l'ecran
    display.display();
    break;

  case 2: //Pendant le transport / Enregistrement des temperatures

    //Vide le buffer contenant ce que l'on afficher précédemment
    display.clearDisplay();

    // setTextSize applique est facteur d'échelle qui permet d'agrandir ou réduire la
    //font
    display.setTextSize(1);

    // La couleur du texte
    display.setTextColor(WHITE);

    // On va écrire en x=0, y=0
    display.setCursor(0, 0);

    // un println comme pour écrire sur le port série
    display.println("12           246");
    display.println("13h25");
    display.println(temp);
    display.println("78%");

    //Rafraichire le cache de l'ecran
    display.display();
    break;

  case 3: //Connexion en bluetooth avec l'application mobile / Arret de l'enregistrement

    //Vide le buffer contenant ce que l'on afficher précédemment
    display.clearDisplay();

    // setTextSize applique est facteur d'échelle qui permet d'agrandir ou réduire la
    //font
    display.setTextSize(1);

    // La couleur du texte
    display.setTextColor(WHITE);

    // On va écrire en x=0, y=0
    display.setCursor(0, 0);

    display.println("12           246");
    display.println("13h25");
    // un println comme pour écrire sur le port série
    display.println(temp);
    display.println("78%");

    //Rafraichire le cache de l'ecran
    display.display();
    break;

  default: //Arret du transport
    //Vide le buffer contenant ce que l'on afficher précédemment
    display.clearDisplay();

    // setTextSize applique est facteur d'échelle qui permet d'agrandir ou réduire la
    //font
    display.setTextSize(1);

    // La couleur du texte
    display.setTextColor(WHITE);

    // On va écrire en x=0, y=0
    display.setCursor(0, 0);

    // un println comme pour écrire sur le port série
    display.println("78%");

    //Rafraichire le cache de l'ecran
    display.display();
  }

  delay(5000);
}
