#include <Wire.h>             //Pour la temperature
#include "temperature.h"

#define TMP102_I2C_ADDRESS 72   //Adresse du capteur i2c
#define SDA_I2C 5
#define SCL_I2C 4

temperature::temperature(){

}

temperature::~temperature(){

}

float temperature::recuperationTemperature(){ 
  //Variable contenant les trames que l'on récupére grace à l'I2C
  unsigned char firstbyte, secondbyte;
  //un int est capable de stocker deux octets, c'est là que nous ajoutons les deux octets ensemble.
  int val;
  //Nous devons ensuite multiplier nos deux octets par 0.0625, mentionné dans la fiche technique.
  float convertedtemp;
  
  //Demarrage de la communication I2C sur la broche 5 et 4
  Wire.begin(SDA_I2C, SCL_I2C);
  
  //Envoie de la requete I2C au TMP102
  Wire.requestFrom(TMP102_I2C_ADDRESS, 2);

  //ici nous lisons un octet de chacun des registres de température sur le TMP102
  firstbyte = (Wire.read());
  secondbyte = (Wire.read());

  val = firstbyte;

  if ((firstbyte & 0x80) > 0) //Si la valeur est négative
  {
    val |= 0x0F00; //forsage à 1 sur le 1er octets
  }

  val = ((val) << 4); //décalage de 4 sur la gauche (MSB)

  val |= (secondbyte >> 4); //Décalage de 4 à droite (LSB)

  //Multiplication par 0.0625 de la valeur de la temperature (voir fiche tech TMP102)
  convertedtemp = val * 0.0625;

  return convertedtemp;

}