#include <Wire.h>             //Pour la temperature
#include <Adafruit_GFX.h>     //Pour l'affichage
#include <Adafruit_SSD1306.h> //Pour l'affichage
#include "affichage.h"

#define SCREEN_WIDTH 128  //Largeur de l'ecran OLED
#define SCREEN_HEIGHT 64  //hauteur de l'ecran OLED
#define SDA_I2C 5
#define SCL_I2C 4

affichage::affichage(){

}

affichage::~affichage(){

}

void affichage::afficherSurEcran(String prmAffichage, String prmAffichage2, String prmAffichage3){
  
  //Parametrage de la biblotheque Adafruit
  Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
  
  //Demarrage de la communication I2C sur la broche 5 et 4
  Wire.begin(SDA_I2C, SCL_I2C);

  //Initialisation de l'affichage et gestion des erreurs
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false))
  {
    Serial.println(F("Configuration de la bibliotheque ADAfruit a echouer"));
  }

  //Vide le buffer contenant ce que l'on afficher précédemment
    display.clearDisplay();

    // setTextSize applique est facteur d'échelle qui permet d'agrandir ou réduire la
    //font
    display.setTextSize(1);

    // La couleur du texte
    display.setTextColor(WHITE);

    // On va écrire en x=0, y=0
    display.setCursor(0, 0);

    // un println comme pour écrire sur le port série
    display.println(prmAffichage);
    display.println(prmAffichage2);
    display.println(prmAffichage3);

    //Rafraichire le cache de l'ecran
    display.display();


}