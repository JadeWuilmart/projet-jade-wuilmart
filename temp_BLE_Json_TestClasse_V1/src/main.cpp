#include "varGlob/varGlob.h"
//---------------------------------Constante----------------------------------------------------------
#define SERVICE_UART_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"      //Identifiant du service (pour le BLE)
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" //Identifiant des characteristiques (pour le BLE)
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" //Identifiant des descriptions (pour le BLE)
#define NOM_SERVICE_BLE "Conteneur 2"
//---------------------------Classe Etat serveur BLE-----------------------------------------
class EtatServeur : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    estConnecte = true; //une personne se connecte
  }

  void onDisconnect(BLEServer *pServer)
  {
    estConnecte = false; //une personne se deconnecte
  }
};
//-------------------Classe permetant de recuperrer et d'afficher une valeur en BLE-----------------
class CharacteristicUART : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristique)
  {
    std::string rxValue = pCharacteristique->getValue(); //recuperation de la valeur envoie en BLE

    //Affichage
    if (rxValue.length() > 0)
    {
      Serial.println("*********");
      Serial.print("Valeur reçu : ");
      //Affichage de la valeur dans la console et copie du message
      for (int i = 0; i < rxValue.length(); i++)
      {
        Serial.print(rxValue[i]);
        messageRecu[i] = rxValue[i];
      }
      Serial.println();
      Serial.println("*********");
      envoieDonnees = true;
    }
  }
};
//------------------------Installation de l'environement d'execution-------------------------
void setup()
{
  //Vitesse de l'envoie des données
  Serial.begin(9600);
  //Demarrage de la communication I2C sur la broche 5 et 4
  Wire.begin(5, 4);

  //Creation d'un peripherique BLE et lui donne un nom
  BLEDevice::init(NOM_SERVICE_BLE);

  //Definition du peripherique BLE en tant que serveur
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new EtatServeur());

  //Creation d'un service pour le serveur BLE avec UUID
  BLEService *pServiceUART = pServer->createService(SERVICE_UART_UUID);

  //Definition du service avec UUID et les propriétés (NOTIFICATION)
  pTxCharacteristic = pServiceUART->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);

  //Ajout de la description utilisateur caractèristique
  pTxCharacteristic->addDescriptor(new BLE2902());

  //Definition du service avec UUID et les propriétés (ECRIRE)
  pRxCharacteristic = pServiceUART->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);

  //Appel de la fonction de lecture (afin de pouvoir recuperer les données que l'on nous envoie)
  pRxCharacteristic->setCallbacks(new CharacteristicUART());

  //Demarre le service
  pServiceUART->start();

  //Numerise le BLE afin que les autres appareils en BLE puissent le trouver
  pServer->getAdvertising()->start();

  Serial.println("Bluetooth demarrer");
  Serial.println("En attente de connexion");
  Serial.println();
}

//-----------------------------------------Boucle d'execution-----------------------------
void loop()
{
  char dhtDataString[10]; //tableau qui vas contenir la temperature afin de l'afficher en BLE
  temperature objTemp;    //obj de la classe temperature
  float temperature;      //variable qui vas contenir la valeur de la temperature
  etatAffichage = 0;

  //obj de la classe affichage
  affichage objAffichage;
  //Paramettre pour la fonction afficherSurEcran
  String prmAffichage = "78%";
  String prmAffichage2 = "";
  String prmAffichage3 = dhtDataString;

  //Initialisation du vecteur dhtDataString
  for (int i = 0; i <= 6; i++)
  {
    dhtDataString[i] = 0;
  }

  //Recuperation de la temperature
  temperature = objTemp.recuperationTemperature();
  Serial.print("Temperature : ");
  Serial.println(temperature);

  //Copie de la temperature dans un tableau
  dtostrf(temperature, 5, 2, dhtDataString);

  //Quand l'esp recoit une valeur en BLE
  if (envoieDonnees == true)
  {
    //obj de la classe jsonArduino
    jsonArduino objJson;
    //Variable pour recuperer les donnees en json
    String nbTransport;
    String mdp;
    String nomUtilisateur;
    String heureDepart;

    //Recuperation des donnees en json
    nbTransport = objJson.recuperationJsonNbTransport(messageRecu);
    mdp = objJson.recuperationJsonMdp(messageRecu);
    nomUtilisateur = objJson.recuperationJsonNomUtilisateur(messageRecu);
    heureDepart = objJson.recuperationJsonHeuredepart(messageRecu);

    Serial.print("Numero transport : ");
    Serial.println(nbTransport);
    Serial.print("MDP : ");
    Serial.println(mdp);
    Serial.print("Identifiant : ");
    Serial.println(nomUtilisateur);
    Serial.print("Heure de depart");
    Serial.println(heureDepart);

    envoieDonnees = false;
  }

  // notification
  if (estConnecte)
  {
    etatAffichage = 1;
    //Valeur de la caractéristique du service BLE
    pTxCharacteristic->setValue(dhtDataString);
    //Envoie de la valeur à l'appareil connecter à l'esp
    pTxCharacteristic->notify();

    delay(500);
  }

  // déconnecté ?
  if (!estConnecte && etaitConnecte)
  {
    //Redemarrage du service
    pServer->startAdvertising();
    Serial.println("Un utilisateur s'est deconnecté");
    Serial.println("Le bluetooth a ete redemarrer");
    Serial.println();

    etaitConnecte = estConnecte;
    etatAffichage = 0;
    delay(500);
  }

  // connecté ?
  if (estConnecte && !etaitConnecte)
  {
    Serial.println();
    Serial.println("Un utilisateur est connecté");
    Serial.println();
    Serial.print(dhtDataString);
    Serial.println();
    
    etaitConnecte = estConnecte;
    etatAffichage = 1;
  }

  if (etatAffichage == 0)
  {
    prmAffichage = "78%";
    prmAffichage2 = " ";
    prmAffichage3 = " ";
    objAffichage.afficherSurEcran(prmAffichage, prmAffichage2, prmAffichage3);
  }

  if (etatAffichage == 1)
  {
    prmAffichage = "12";
    prmAffichage2 = "Wuilmart";
    prmAffichage3 = "78%";
    objAffichage.afficherSurEcran(prmAffichage, prmAffichage2, prmAffichage3);
  }

  if (etatAffichage == 2)
  {
    prmAffichage = "12        246";
    prmAffichage2 = dhtDataString;
    prmAffichage3 = "78%";
    objAffichage.afficherSurEcran(prmAffichage, prmAffichage2, prmAffichage3);
  }

  if (etatAffichage == 3)
  {
    prmAffichage = "Transfert en cour";
    prmAffichage2 = dhtDataString;
    prmAffichage3 = "78%";
    objAffichage.afficherSurEcran(prmAffichage, prmAffichage2, prmAffichage3);
  }

  delay(5000);
}
