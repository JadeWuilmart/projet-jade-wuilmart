//----------------------------------Bibliothèque---------------------------------------------------
#include <BLEDevice.h> //Pour le BLE
#include <BLEServer.h> //Pour le BLE
#include <BLEUtils.h>  //Pour le BLE
#include <BLE2902.h>   //Pour le BLE
#include <Wire.h>      //Pour la temperature
#include "temperature/temperature.h"
#include "affichage/affichage.h"
#include "jsonArduino/jsonArduino.h"
//-----------------------------------Variable Globale------------------------------------------------
bool etaitConnecte = false;
bool envoieDonnees = false;
bool estConnecte = false;
int etatAffichage;
String messageRecu[100];
//------------------------------Création du pointeur pour le BLE-------------------------------------
BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic = NULL;
BLECharacteristic *pRxCharacteristic = NULL;