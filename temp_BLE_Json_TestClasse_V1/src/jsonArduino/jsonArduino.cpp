#include <ArduinoJson.h> //Pour JSON
#include "jsonArduino.h"

jsonArduino::jsonArduino(){

}

jsonArduino::~jsonArduino(){

}

String jsonArduino::recuperationJsonNomUtilisateur(String prmMessage[100]){
    StaticJsonBuffer<300> JsonBufferConfig;
    JsonObject &parsed = JsonBufferConfig.parseObject(prmMessage);
    String nomUtilisateur = parsed["identifiant"];

    return nomUtilisateur;

}
String jsonArduino::recuperationJsonNbTransport(String prmMessage[100]){
    StaticJsonBuffer<300> JsonBufferConfig;
    JsonObject &parsed = JsonBufferConfig.parseObject(prmMessage);
    String nbTransport = parsed["numTransport"];

    return nbTransport;
}

String jsonArduino::recuperationJsonMdp(String prmMessage[100]){
    StaticJsonBuffer<300> JsonBufferConfig;
    JsonObject &parsed = JsonBufferConfig.parseObject(prmMessage);
    String mdp = parsed["mpVerifContainer"];
 
    return mdp ; 
}

String jsonArduino::recuperationJsonHeuredepart(String prmMessage[100]){
    StaticJsonBuffer<300> JsonBufferConfig;
    JsonObject &parsed = JsonBufferConfig.parseObject(prmMessage);
    String Heuredepart = parsed["heuredepart"];

    return Heuredepart;

}
